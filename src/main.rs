extern crate config;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate crossterm;

use std::collections::HashMap;
use crate::conf::Settings;
use std::borrow::Borrow;

mod conf;
mod commits;
mod termui;
mod readlineui;

lazy_static! {
    pub static ref CONFIG: conf::Settings = conf::Settings::load().unwrap();
}

pub fn main() {
    termui::term().unwrap();
}

