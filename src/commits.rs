use crate::conf::{CommitType, IssueScope};
use std::collections::HashMap;
use tera::Context;
use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Issue {
    id: String,
    id_readable: String,
    summary: String,
    description: Option<String>
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Commit {
    kind: CommitType,
    summary: String,

    scope: Option<IssueScope>,
    issue: Option<Issue>,
    long_description: Option<String>
}

pub struct CommitBuilder {
    kind: CommitType,
    summary: String,

    scope: Option<IssueScope>,
    issue: Option<Issue>,
    long_description: Option<String>
}

impl CommitBuilder {
    pub fn in_scope(&mut self, scope: IssueScope) -> &mut Self {
        self.scope = Some(scope);
        self
    }

    pub fn for_issue(&mut self, issue: Issue) -> &mut Self {
        self.issue = Some(issue);
        self
    }

    pub fn with_description(&mut self, description: String) -> &mut Self {
        self.long_description = Some(description);
        self
    }
}

impl Commit {
    pub fn builder(kind: CommitType, summary: String) -> CommitBuilder {
        CommitBuilder {
            kind,
            summary,

            scope: None,
            issue: None,
            long_description: None
        }
    }

    pub fn kind(&self) -> &CommitType {
        &self.kind
    }

    pub fn summary(&self) -> &String {
        &self.summary
    }

    pub fn scope(&self) -> &Option<IssueScope> {
        &self.scope
    }

    pub fn issue(&self) -> &Option<Issue> {
        &self.issue
    }

    pub fn long_description(&self) -> &Option<String> {
        &self.long_description
    }

    pub fn context(&self) -> Context {
        let mut context = Context::new();
        context.insert("Kind", self.kind());
        context.insert("Summary", self.summary());

        match self.scope() {
            Some(scope) => {
                context.insert("Scope", scope)
            }
            None => {}
        }

        match self.issue() {
            Some(issue) => {
                context.insert("Issue", issue);
            }
            None => {}
        }

        match self.long_description() {
            Some(desc) => {
                context.insert("Description", desc);
            }
            None => {}
        }

        context
    }
}

