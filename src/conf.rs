
use config::{Config, ConfigError, Environment, File};
use std::collections::HashSet;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Settings {
    #[serde(alias = "tracking-db")]
    tracking_db: IssueStorage,
    #[serde(alias = "auto-bump")]
    auto_bump: bool,
    // TODO change it to set
    #[serde(alias = "commit-types")]
    commit_types: Vec<CommitType>,
    #[serde(alias = "issue-scopes", alias = "scopes")]
    issue_scopes: Vec<IssueScope>,
    #[serde(alias = "templates", alias = "templates-path")]
    templates: Templates
}

impl Settings {
    pub fn load() -> Result<Self, ConfigError> {
        Self::load_from("conf/commie.toml")
    }

    pub fn load_from(path: &str) -> Result<Self, ConfigError> {
        let mut cfg: Config = Config::new();

        cfg.merge(File::with_name(path));

        cfg.try_into()
    }

    pub fn commit_types(&self) -> &Vec<CommitType> {
        &self.commit_types
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum IssueStorage {
    None,
    InsideProject {
        filename: String,
    },
    PredefinedPath {
        root: String,
        pattern: String
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct IssueScope {
    name: String,
    description: String,
}

impl IssueScope {
    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn description(&self) -> &String {
        &self.description
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct  CommitType {
    name: String,
    description: String,
    bump: BumpVersion
}

impl CommitType {
    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn description(&self) -> &String {
        &self.description
    }

    pub fn bump(&self) -> &BumpVersion {
        &self.bump
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub enum BumpVersion {
    #[serde(alias = "none")]
    None,
    #[serde(alias = "major")]
    Major,
    #[serde(alias = "minor")]
    Minor,
    #[serde(alias = "patch")]
    Patch
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum Templates {
    #[serde(alias = "custom")]
    CustomPath {
        dir: String,
        suffix: Option<String>
    },
    #[serde(alias = "config")]
    ConfigDir {
        subdir: Option<String>,
        suffix: Option<String>
    },
    #[serde(alias = "default")]
    Default
}
