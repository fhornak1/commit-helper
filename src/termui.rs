use std::{io::{stdout, Write}, sync::mpsc, thread, time::Duration, io};


use crossterm::{event::{self, Event as CEvent, KeyCode, KeyEvent, KeyModifiers}, execute, terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen}, Command, ErrorKind};

use tui::{backend::CrosstermBackend, Terminal, Frame};
use tui::backend::Backend;
use tui::layout::{Layout, Constraint, Rect};
use tui::widgets::{Tabs, Block, Widget, Borders, Paragraph, Text};
use tui::style::{Style, Color, Modifier};

enum Event<I> {
    Tick,
    Input(I)
}

pub fn term() -> Result<(), failure::Error> {
    enable_raw_mode()?;

    let mut stdout = stdout();

    execute!(stdout, EnterAlternateScreen)?;

    let backend = CrosstermBackend::new(stdout);

    let mut terminal = Terminal::new(backend)?;

    terminal.hide_cursor()?;

    let (tx, rx) = mpsc::channel();

    let mut app = App::new("Commie UI");

    terminal.show_cursor()?;

    thread::spawn(move || {
        loop {
            if event::poll(Duration::from_millis(50)).unwrap() {
                if let CEvent::Key(c) = event::read().unwrap() {
                    tx.send(Event::Input(c)).unwrap();
                }
            } else {
                tx.send(Event::Tick).unwrap();
            }
        }
    });

    terminal.clear()?;

    loop {
        draw(&mut terminal, &app);
        match rx.recv()? {
            Event::Input(KeyEvent { code: KeyCode::Esc, modifiers}) => {
                disable_raw_mode()?;
                let t = terminal.backend_mut();
                handle_command!(t, LeaveAlternateScreen)?;
                tui::backend::Backend::flush(t).map_err(ErrorKind::IoError)?;
                terminal.show_cursor()?;
                break;
            },
            Event::Input(KeyEvent { code: KeyCode::Char(c), modifiers} ) => app.on_key(c),
            Event::Input(KeyEvent { code: KeyCode::Tab, modifiers }) => app.on_right(),
            Event::Input(KeyEvent { code: KeyCode::BackTab, modifiers}) => app.on_left(),
            Event::Input(KeyEvent { code: KeyCode:: Backspace, modifiers }) => {
                if app.remove_commit_char() {
                    let (column, row) = terminal.get_cursor()?;
                    terminal.set_cursor(column - 1, row)?;
                }
            },
            Event::Tick => app.on_tick(),
            _ => {}
        }
    }

    Ok(())
}

fn draw<B: Backend>(terminal: &mut Terminal<B>, app: &App) -> Result<(), io::Error> {
    terminal.draw(|mut frame| {
        let chunks = Layout::default()
            .constraints([Constraint::Length(3), Constraint::Min(0)].as_ref())
            .split(frame.size());
        Tabs::default()
            .block(Block::default().borders(Borders::ALL).title(app.title))
            .titles(&app.tabs.items)
            .style(Style::default().fg(Color::Green).bg(Color::Black))
            .highlight_style(Style::default().fg(Color::Yellow))
            .select(app.tabs.selected)
            .render(&mut frame, chunks[0]);
        match app.tabs.selected {
            0 => draw_commit_message(&mut frame, app, chunks[1]),
            1 => {},
            _ => {}
        }
    })
}

fn draw_commit_message<B>(frame: &mut Frame<B>, app: &App, area: Rect)
where B: Backend
{
    let chunks = Layout::default()
        .constraints(
            [
                Constraint::Length(7),
                Constraint::Min(7),
                Constraint::Length(7)
            ]
                .as_ref()
        )
        .split(area);

    Paragraph::new(
        [
            Text::Raw(std::borrow::Cow::from(&app.commit_buf)),
        ].iter()
    )
        .block(
            Block::default()
                .title("Commit Summary")
                .title_style(Style::default().fg(Color::Magenta))
                .borders(Borders::ALL)
                .border_style(Style::default().fg(Color::Magenta))
        )
        .wrap(true)
        .render(frame, chunks[0]);
}

struct App<'a> {
    title: &'a str,
    tabs: ListStates<&'a str>,
    changes: ListStates<Change<'a>>,
    commit_buf: String
}

impl <'a> App<'a> {
    pub fn new(title: &'a str) -> Self {
        Self {
            title,
            tabs: ListStates::with_items(vec!["Commit Message", "Changes", "Issue Description"]),
            changes: ListStates::with_items(vec![]),
            commit_buf: String::with_capacity(80),
        }
    }

    pub fn on_right(&mut self) {
        self.tabs.next()
    }

    pub fn on_left(&mut self) {
        self.tabs.previous()
    }

    pub fn on_key(&mut self, key: char) {
        self.commit_buf.push(key);
    }

    pub fn remove_commit_char(&mut self) -> bool {
        match self.commit_buf.pop() {
            Some(_) => true,
            None => false
        }
    }

    pub fn on_tick(&mut self) {
    }
}

pub struct TextArea {
    lines: Vec<String>,
    line_no: usize,
    line_pos: usize,
    style: Style
}

impl TextArea {
    pub fn new() -> Self {
        Self {
            lines: vec![String::with_capacity(80)],
            line_no: 0,
            line_pos: 0,
            style: Style::default()
        }
    }

    pub fn style(&mut self, style: Style) -> &mut Self {
        self.style = style;
        self
    }

    pub fn left(&mut self) {
        if self.line_pos > 0 {
            self.line_pos -= 1;
        }
    }

    pub fn right(&mut self) {
        let new_pos = self.line_pos + 1;
        if new_pos < self.lines[self.line_no].len() {
            self.line_pos = new_pos
        }
    }

    pub fn down(&mut self) {
        let new_line = self.line_no + 1;
        if new_line < self.lines.len() {
            self.line_no = new_line;
        }
    }

    pub fn up(&mut self) {
        if self.line_no > 0 {
            self.line_no -= 1;
        }
    }
}

pub trait Tab {
    fn on_event(&mut self, event: &CEvent);

    fn name(&self) -> &str;
}

pub struct TabsElement {
    tabs: Vec<Box<dyn Tab>>,
    selected: usize
}

impl TabsElement {
    pub fn of(tabs: Vec<Box<dyn Tab>>) -> Self {
        Self {
            tabs,
            selected: 0
        }
    }

    pub fn names(&self) -> Vec<&str> {
        self.tabs.iter().map(|s| s.name()).collect()
    }

    pub fn on_event(&mut self, evt: &CEvent) {
        match evt {
            &CEvent::Key(KeyEvent {code: KeyCode::Tab, modifiers: KeyModifiers::CONTROL }) => {
                self.next()
            },
            &CEvent::Key(KeyEvent { code: KeyCode::Tab, modifiers: KeyModifiers::SHIFT }) => {
                self.previous()
            },
            _ => self.tabs[self.selected].on_event(evt)
        }
    }

    pub fn next(&mut self) {
        self.mv(self.selected + 1)
    }

    pub fn previous(&mut self) {
        self.mv(self.selected - 1 + self.tabs.len())
    }

    fn mv(&mut self, shift: usize) {
        self.selected = shift % self.tabs.len()
    }
}

pub struct ListStates<I> {
    items: Vec<I>,
    selected: usize
}

impl <I> ListStates<I> {
    pub fn with_items(items: Vec<I>) -> Self {
        Self {
            items,
            selected: 0
        }
    }

    pub fn next(&mut self) {
        self.jump_mod(self.selected + 1)
    }

    pub fn previous(&mut self) {
        self.jump_mod(self.selected + self.items.len() - 1)
    }

    fn jump_mod(&mut self, pos: usize) {
        self.selected = pos % self.items.len();
    }
}

pub enum Change<'a> {
    Tracked {
        modification: Modification<'a>,
        add_to_commit: bool
    },
    Untracked {
        filepath: &'a str,
        add_to_commit: bool
    },
    Ignored {
        filepath: &'a str,
        add_exception: bool,
        add_to_commit: bool
    },
}

pub enum Modification<'a> {
    Modified {
        prev: &'a str,
        current: &'a str
    },
    Created {
        current: &'a str
    },
    Deleted {
        prev: &'a str,
    },
    Moved {
        prev: &'a str,
        current: &'a str
    }
}
